//override defaults
if (alertify) {
    alertify.defaults.glossary.title = "Thông báo";
    alertify.defaults.glossary.ok = "Đóng";
    alertify.defaults.glossary.cancel = "Hủy";
    alertify.defaults.transition = "slide";
    alertify.defaults.theme.ok = "btn btn-primary";
    alertify.defaults.theme.cancel = "btn btn-danger";
    alertify.defaults.theme.input = "form-control";
    alertify.defaults.hooks.preinit = function() {
        // before init dialog
        console.log("- before init dialog");
    };
    alertify.defaults.hooks.postinit = function() {
        // after init dialog
        console.log("- after init dialog");
    };
} else {
    console.log("Missing alertify!");
}
window.showAlert = function(content) {
    alertify.alert(content).set({ 'labels': { 'ok': 'Đóng' }, 'padding': false }).setting('modal', true);
}
window.showDialog = function(content, callback) {
    alertify.alert(content).set({ 'resizable': true, 'closable': true, 'title': ' &nbsp;' }).setting({
        'modal': true,
        //'frameless': true,
        'label': 'Đóng',
        'onok': callback
    }).resizeTo('70%');
}
window.showInfo = function(content) {
    alertify.notify(content);
}
window.showWarning = function(content) {
    alertify.warning(content);
}
window.showSuccess = function(content) {
    alertify.success(content);
}
window.showError = function(content) {
    alertify.error(content);
}
window.showConfirm = function(content, callback) {
        alertify.confirm(content, function() {
            //alertify.success('Accepted');
            if (callback) callback(true);
        }, function() {
            //alertify.error('Declined');
            if (callback) callback(false);
        }).set({ labels: { 'ok': 'Đồng ý', 'cancel': 'Không' }, 'padding': false }).setting('modal', true);
    }
    // override browser alert
window.alert = window.showAlert;
//window.confirm = window.showConfirm;