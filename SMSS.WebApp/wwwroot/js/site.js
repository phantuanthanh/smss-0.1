﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.
// Write your JavaScript code.
$.fn.serializeObject = function () {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        if (o[this.name]) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

// ajax global handlers
$(document).ajaxError(function (e, x, settings, exception) {
    var message;
    var statusErrorMap = {
        '400': "Server understood the request, but request content was invalid.",
        '401': "Unauthorized access. Please login.",
        '403': "Forbidden resource can't be accessed.",
        '500': "Internal server error.",
        '503': "Service unavailable."
    };
    if (x.status) {
        message = statusErrorMap[x.status];
        if (!message) {
            message = "Unknown Error \n.";
        }
    } else if (exception == 'parsererror') {
        message = "Error.\nParsing JSON Request failed.";
    } else if (exception == 'timeout') {
        message = "Request Time out.";
    } else if (exception == 'abort') {
        message = "Request was aborted by the server";
    } else {
        message = "Unknown Error \n.";
    }
    window.showError(message);
});

// Gets or sets the name of the JavaScript function to call immediately before the page is updated.
window.beginRequestHandler = function (evt, form, message) {
    try {
        $("button[type=submit]", form).prop("disabled", true);
        if (message) showInfo(message);
    }
    catch (ex) { }
};

// Gets or sets the JavaScript function to call after the page is successfully updated.
window.successRequestHandler = function (evt, form, message) {
    try {
        if (message) showInfo(message);
    }
    catch (ex) { }
};

// Gets or sets the JavaScript function to call if the page update fails.
window.failureRequestHandler = function (evt, form, message) {
    try {
        if (message) showError(message);
    }
    catch (ex) { }
};

// Gets or sets the JavaScript function to call when response data has been instantiated but before the page is updated.
window.completeRequestHandler = function (evt, form, message) {
    //console.log(evt);
    try {
        $("button[type=submit]", form).prop("disabled", false);
        let response = JSON.parse(evt.target.response);
        if (response.errorCode) {
            showError('' + response.errorMessages);
        }
        else {
            if (message) showSuccess(message);
        }
    }
    catch (ex) {}
};


// Gets or sets the name of the JavaScript function to call immediately before the page is updated.
if (!window.getFullAddress) {
    window.getFullAddress = function (addressFormSelector, formData) {
        console.log(addressFormSelector);
        let objForm = $(addressFormSelector);
        if (!objForm.length) {
            console.warn(addressFormSelector + " not found.");
            return;
        }
        if (!formData) formData = objForm.serializeObject();

        let countryName = '';
        if (!$("select[name=CountryId]", objForm).length) {
            console.warn("select[name=CountryId] not found.");
            return;
        } else {
            if (formData.CountryId)
                countryName = ', ' + $("select[name=CountryId] option:selected", objForm).text();
        }

        let provinceName = '';
        if (!$("select[name=ProvinceId]", objForm).length) {
            console.warn("select[name=ProvinceId] not found.");
            return;
        }
        else {
            if (formData.ProvinceId)
                provinceName = ', ' + $("select[name=ProvinceId] option:selected", objForm).text();
        }

        let districtName = '';
        if (formData.DistrictId && $("select[name=DistrictId]", objForm).length) {
            districtName = $("select[name=DistrictId] option:selected", objForm).text();
        }

        let wardName = '';
        if (formData.WardId && $("select[name=WardId]", objForm).length) {
            wardName = $("select[name=WardId] option:selected", objForm).text();
        }

        let fullAddress = formData.Street + ' ' + wardName + ' ' + districtName + provinceName + countryName;

        return fullAddress;
    };
}
